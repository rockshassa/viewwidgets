//
//  CGNumberIndicatorView.m
//  ViewWidgets
//
//  Created by Nicholas Galasso on 3/19/13.
//  Copyright (c) 2013 Nicholas Galasso. All rights reserved.
//

#import "CGNumberIndicatorView.h"

@implementation CGNumberIndicatorView

#pragma mark - Getters and Setters

-(NSString*)displayedString{
    return [NSString stringWithFormat:@"%i", _displayedInt];
}

-(void)setDisplayedInt:(int)displayedInt{
    
    _displayedInt = displayedInt;
    
    [self setNeedsDisplay];
}

- (id)initWithFrame:(CGRect)frame Count:(int)count
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        _textLabel.textColor = [UIColor blackColor];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.font = [UIFont fontWithName:_textLabel.font.fontName size:20];
        [self addSubview:_textLabel];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)layoutSubviews{
    [self drawRect:self.frame];
}

- (void)drawRect:(CGRect)rect
{
    // constants
//    const CGFloat outlineStrokeWidth = 20.0f;
//    const CGFloat outlineCornerRadius = 15.0f;
//    
//    const CGColorRef whiteColor = [[UIColor whiteColor] CGColor];
//    const CGColorRef redColor = [[UIColor redColor] CGColor];
//    
//    // get the context
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    // set the background color to white
//    CGContextSetFillColorWithColor(context, whiteColor);
//    CGContextFillRect(context, rect);
//    
//    // inset the rect because half of the stroke applied to this path will be on the outside
//    CGRect insetRect = CGRectInset(rect, outlineStrokeWidth/2.0f, outlineStrokeWidth/2.0f);
//    
//    // get our rounded rect as a path
//    CGPathRef path = [UIBezierPath bezierPathWithRoundedRect:insetRect cornerRadius:outlineCornerRadius].CGPath;
//     
//    // add the path to the context
//    CGContextAddPath(context, path);
//    
//    // set the stroke params
//    CGContextSetStrokeColorWithColor(context, redColor);
//    CGContextSetLineWidth(context, outlineStrokeWidth);
//    
//    // draw the path
//    CGContextDrawPath(context, kCGPathStroke);
//    
//    // release the path
//    CGPathRelease(path);
    
    
    _textLabel.text = [NSString stringWithFormat:@" %i new ", _displayedInt];
    [_textLabel sizeToFit];
    
    self.frame = [self convertRect:_textLabel.frame fromView:_textLabel];
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.frame cornerRadius:10];
    
    [[UIColor cyanColor] setFill];
    [path fill];
}



-(void)incrementCount{
    self.displayedInt = _displayedInt+1;
}

-(void)decrementCount{
    self.displayedInt = _displayedInt-1;
}


@end
