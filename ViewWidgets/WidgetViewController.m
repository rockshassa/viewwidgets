//
//  WidgetViewController.m
//  ViewWidgets
//
//  Created by Nicholas Galasso on 3/18/13.
//  Copyright (c) 2013 Nicholas Galasso. All rights reserved.
//

#import "WidgetViewController.h"
#import "NumberIndicatorView.h"
#import "CGNumberIndicatorView.h"

@interface WidgetViewController ()

@property (nonatomic, strong) NumberIndicatorView *numView;

@end

@implementation WidgetViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView{
    
    UIView *incView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
    incView.backgroundColor = [UIColor lightGrayColor];
    
    UISegmentedControl *seg = [[UISegmentedControl alloc] initWithItems:@[@"1", @"10", @"100"]];
    [seg addTarget:self action:@selector(tappedSegment:) forControlEvents:UIControlEventValueChanged];
    seg.frame = CGRectMake(50, 350, 200, 50);
    [incView addSubview:seg];
    
    self.view = incView;
}

-(void)tappedSegment:(UISegmentedControl*)sender{
    
    switch (sender.selectedSegmentIndex) {
        case 0:
            _numView.displayedInt = 1;
            break;
        case 1:
            _numView.displayedInt = 10;
            break;
        case 2:
            _numView.displayedInt = 100;
            break;
        default:
            break;
    }
    
    sender.selectedSegmentIndex = -1;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    UIImage *img = [UIImage imageNamed:@"blue"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 10, 10, 10);
    img = [img resizableImageWithCapInsets:insets];
    
    _numView = [[NumberIndicatorView alloc] initWithImage:img Count:10];
    [self.view addSubview:_numView];
    
    UIButton *incBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    incBtn.frame = CGRectMake(200, 250, 50, 50);
    [incBtn setTitle:@"+" forState:UIControlStateNormal];
    [incBtn addTarget:_numView action:@selector(incrementCount) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:incBtn];
    
    UIButton *decBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    decBtn.frame = CGRectMake(50, 250, 50, 50);
    [decBtn setTitle:@"-" forState:UIControlStateNormal];
    [decBtn addTarget:_numView action:@selector(decrementCount) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:decBtn];
    
}

-(void)viewWillLayoutSubviews{
    
    _numView.center = CGPointMake(150, 150);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
