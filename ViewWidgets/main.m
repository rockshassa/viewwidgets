//
//  main.m
//  ViewWidgets
//
//  Created by Nicholas Galasso on 3/18/13.
//  Copyright (c) 2013 Nicholas Galasso. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NGAppDelegate class]));
    }
}
