//
//  CGNumberIndicatorView.h
//  ViewWidgets
//
//  Created by Nicholas Galasso on 3/19/13.
//  Copyright (c) 2013 Nicholas Galasso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CGNumberIndicatorView : UIView

@property (nonatomic, readonly) NSString *displayedString;
@property (nonatomic, assign) int displayedInt;
@property (nonatomic, strong, readonly) UILabel *textLabel;

- (id)initWithFrame:(CGRect)frame Count:(int)count;

-(void)incrementCount;
-(void)decrementCount;

@end
