//
//  NumberIndicatorView.m
//  ViewWidgets
//
//  Created by Nicholas Galasso on 3/18/13.
//  Copyright (c) 2013 Nicholas Galasso. All rights reserved.
//

#import "NumberIndicatorView.h"
#import <QuartzCore/QuartzCore.h>
@interface NumberIndicatorView ()

@property (nonatomic, assign) BOOL isAnimating;
@property (nonatomic, assign) BOOL isInitializing;
@property (nonatomic, strong) UIImage *ovalImg;

@end

@implementation NumberIndicatorView

#pragma mark - Getters and Setters

-(NSString*)displayedString{
    return [NSString stringWithFormat:@" %i new ", _displayedInt];
}

-(void)setDisplayedInt:(int)displayedInt{
    
    _displayedInt = displayedInt;

    [self setNeedsLayout];
}

- (id)initWithImage:(UIImage *)image Count:(int)count
{
    self = [super initWithImage:image];
    if (self) {
        
        _isAnimating = NO;
        _isInitializing = YES;
        
        // Initialization code
        _displayedInt = count;
        
//        NSLog(@"%@", NSStringFromCGRect(self.frame));
        self.backgroundColor = [UIColor clearColor];
        
        
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.font = [UIFont fontWithName:_textLabel.font.fontName size:50];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
//        self.layer.borderColor  = [UIColor whiteColor].CGColor;
//        self.layer.borderWidth  = 1.0f;
        
//        NSLog(@"%@", NSStringFromCGPoint(self.center));
//        NSLog(@"%@", NSStringFromCGPoint(_textLabel.center));
        
        [self addSubview:_textLabel];
        
    }
    return self;
}

-(void)layoutSubviews{
    
    /*
     When we set self.frame further down in this method,
     layoutSubviews will get called a 2nd time, before 
     animations are completed for the first call. This can
     cause the UI to hang. use the isAnimating bool to ignore
     calls to layoutSubviews while we are animating.
     
     When this view is initialized, we do not want to animate
     view setup, so set animation duration to 0; Not doing this
     can cause the UI to hang.
     */
    
    
    if (_isAnimating) {
        return;
    } else {
        _isAnimating = YES;
    }
    
    [super layoutSubviews];
    
    NSTimeInterval duration = .25;
    
    if (_isInitializing) {
        duration = 0;
    }
    
    //normal animation
    if (_displayedInt > 0) {
        
        BOOL wasHidden = self.hidden;
        if(wasHidden){
            self.hidden = NO;
            self.alpha = 0;
        }
        
        [UIView animateWithDuration:duration animations:^(void){
            
            NSLog(@"block 1");
            
            if(wasHidden){
                _textLabel.text = self.displayedString;
                self.alpha = 1;
                
            } else {
                _textLabel.alpha = 0;
            }
            
        } completion:^(BOOL finished){
        
            if (finished){
            
                [UIView animateWithDuration:duration animations:^(void){
                    
                    NSLog(@"block 22");
                    _textLabel.text = self.displayedString;
                    [_textLabel sizeToFit];
                    
                    //adjust size of self, without making the origin TOO out of whack
                    CGRect newFrame = self.frame;
                    newFrame.size = _textLabel.frame.size;
                    
                    self.frame = newFrame;
                    
                } completion:^(BOOL finished2){
                
                    if (finished2) {
                        
                        [UIView animateWithDuration:duration animations:^(void){
                            
                            
                            NSLog(@"block 333");
                            _textLabel.alpha = 1;
                            
                        } completion:^(BOOL finished3){
                            _isAnimating = NO;
                        }];
                    }
                    
                }];
                }
        }];
    
    //make bubble dissapear
    } else {
        
        //fade out text
        [UIView animateWithDuration:duration animations:^(void){
            
            _textLabel.alpha = 0;
            
        } completion:^(BOOL finished){
            
            //shrink bubble vertically
            [UIView animateWithDuration:duration
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 self.transform = CGAffineTransformScale(self.transform, 1, 0.2);
                             }
                             completion:^(BOOL finished) {
                                 
                                 //shrink bubble horizontally
                                 [UIView animateWithDuration:duration
                                                       delay:0.0
                                                     options:UIViewAnimationOptionCurveEaseOut
                                                  animations:^{
                                                      self.transform = CGAffineTransformScale(self.transform, 0, 0);
                                                  }
                                                  completion:^(BOOL finished) {
                                                      
                                                      self.hidden = YES;
                                                      self.transform = CGAffineTransformIdentity;
                                                      _textLabel.alpha = 1;
                                                      _isAnimating = NO;
                                                      
                                                  }];

                             }];

        
        }];
    }
    
    _isInitializing = NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)incrementCount{
    self.displayedInt = _displayedInt+1;
}

-(void)decrementCount{
    self.displayedInt = _displayedInt-1;
}

@end
