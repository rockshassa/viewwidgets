//
//  NumberIndicatorView.h
//  ViewWidgets
//
//  Created by Nicholas Galasso on 3/18/13.
//  Copyright (c) 2013 Nicholas Galasso. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 It is reccommended to define the location of this view
 by setting the centerpoint in it's controllers viewWillLayoutSubviews
 method. This will keep the view centered as it resizes.
 */

@interface NumberIndicatorView : UIImageView

@property (nonatomic, readonly) NSString *displayedString;
@property (nonatomic, assign) int displayedInt;
@property (nonatomic, strong, readonly) UILabel *textLabel;

- (id)initWithImage:(UIImage *)image Count:(int)count;

-(void)incrementCount;
-(void)decrementCount;

@end
